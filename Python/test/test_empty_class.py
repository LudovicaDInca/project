from unittest import TestCase

import src.empty_class as empty_class


class TestEmptyClass(TestCase):
    def test_empty_method(self):
        empty_object = empty_class.EmptyClass()

        try:
            self.assertEqual(empty_object.empty_method(), True)
        except Exception as ex:
            self.fail()
class TestNewClass(TestCase):
    def test_new_method(self):
        new_object = new_class.NewClass()