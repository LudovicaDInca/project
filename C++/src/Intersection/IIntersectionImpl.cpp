#include "IIntersectionImpl.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

//double Intersection::toleranceIntersection = 1.0E-7;

Intersection::Intersection()
{
    toleranceIntersection = 1.0E-7;
    type = Intersection::NoIntersection;
    resultParametricCoordinates.setZero(2);

}
Intersection::~Intersection()
{

}

bool Intersection::ComputeIntersection()
{
    double parallelism = lineTangent.determinant();
    type = NoIntersection;
    bool intersection = false;

    double check = toleranceIntersection * toleranceIntersection * lineTangent.col(0).squaredNorm() *  lineTangent.col(1).squaredNorm();
    if(parallelism * parallelism >= check)
    {
        //calcolo delle coordinate parametriche
        Matrix2d solverMatrix;
        solverMatrix << lineTangent(1,1), -lineTangent(0,1), -lineTangent(1,0), lineTangent(0,0);
        resultParametricCoordinates = solverMatrix * rightHandSide;
        resultParametricCoordinates /= parallelism;
        //intersezione nella retta
        if (resultParametricCoordinates(1) > -toleranceIntersection  && resultParametricCoordinates(1)-1.0 < toleranceIntersection)
        {
            type = IntersectionOnLine;
            intersection = true;
            if (resultParametricCoordinates(0) > -toleranceIntersection  && resultParametricCoordinates(0)-1.0 < toleranceIntersection)
                type = IntersectionOnSegment;
        }
        return true;
    }
    else

    {

        {

            double parallelism2 = fabs(lineTangent(0,0) * rightHandSide.y() - rightHandSide.x() * lineTangent(1,0));
            /// <li> In case of parallelism check if the segment is the same with parametric coordinates

            double squaredNormFirstEdge = lineTangent.col(0).squaredNorm();
            double check2 = toleranceIntersection * toleranceIntersection * squaredNormFirstEdge * rightHandSide.squaredNorm();
            if( parallelism2 * parallelism2 <= check2 )
            {

                double tempNorm = 1.0/squaredNormFirstEdge;
                // parametric coordinates on the trace of the starting point and end point
                resultParametricCoordinates(0) = lineTangent.col(0).dot(rightHandSide) * tempNorm ;
                // -1 perchè nella matrice c'è il segno negativo nella seconda colonna
                resultParametricCoordinates(1) = resultParametricCoordinates(0) - lineTangent.col(0).dot(lineTangent.col(1)) * tempNorm;

                intersection = true;
                type = IntersectionParallelOnLine;

                if(resultParametricCoordinates(1) < resultParametricCoordinates(0))
                {
                    double tmp = resultParametricCoordinates(0);
                    resultParametricCoordinates(0) = resultParametricCoordinates(1);
                    resultParametricCoordinates(1) = tmp;
                }
                // if one vertex is in the edge there is the intersection
                if( (resultParametricCoordinates(0) > -toleranceIntersection && resultParametricCoordinates(0)-1.0 < toleranceIntersection) ||
                    (resultParametricCoordinates(1) > -toleranceIntersection && resultParametricCoordinates(1)-1.0 < toleranceIntersection)   )
                    type = IntersectionParallelOnSegment;
                else
                {
                    //IL PRIMO SEGMENTO DATO IN INPUT E' CONTENUTO NEL SECONDO
                    if( ( resultParametricCoordinates(0) < toleranceIntersection && resultParametricCoordinates(1) - 1.0 > -toleranceIntersection) )
                        type = IntersectionParallelOnSegment;
                }
            }
        }

    }


    if(resultParametricCoordinates(0) < -toleranceIntersection || resultParametricCoordinates(0) > 1.0 + toleranceIntersection)
        positionIntersectionFirstEdge =  Outer;
    else if((resultParametricCoordinates(0) > -toleranceIntersection) && (resultParametricCoordinates(0) < toleranceIntersection))
    {
        resultParametricCoordinates(0) = 0.0;
        positionIntersectionFirstEdge = Begin;
    }
    else if ((resultParametricCoordinates(0) > 1.0 - toleranceIntersection) && (resultParametricCoordinates(0) < 1.0 + toleranceIntersection))
    {
        resultParametricCoordinates(0) = 1.0;
        positionIntersectionFirstEdge = End;
    }
    else
        positionIntersectionFirstEdge = Inner;

    if(resultParametricCoordinates(1) < -toleranceIntersection || resultParametricCoordinates(1) > 1.0 + toleranceIntersection)
        positionIntersectionSecondEdge =  Outer;
    else if((resultParametricCoordinates(1) > -toleranceIntersection) && (resultParametricCoordinates(1) < toleranceIntersection))
    {
        resultParametricCoordinates(1) = 0.0;
        positionIntersectionSecondEdge = Begin;
    }
    else if ((resultParametricCoordinates(1) > 1.0 - toleranceIntersection) && (resultParametricCoordinates(1) <= 1.0 + toleranceIntersection))
    {
        resultParametricCoordinates(1) = 1.0;
        positionIntersectionSecondEdge = End;
    }
    else
        positionIntersectionSecondEdge = Inner;

    return intersection;

}

void Intersection::IntersectionPoint(const Vector2d &_originSeg, const Vector2d &_endSeg)
{
    originSeg = _originSeg;
    endSeg = _endSeg;

    pointInt(0) = originSeg(0) + (endSeg(0)-originSeg(0))*resultParametricCoordinates(0);
    pointInt(1) = originSeg(1) + (endSeg(1)-originSeg(1))*resultParametricCoordinates(0);

    intersectionPoints.push_back(pointInt);

}

