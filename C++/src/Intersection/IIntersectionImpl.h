#ifndef POLYGONCUT_IINTERSECTIONIMPL_H
#define POLYGONCUT_IINTERSECTIONIMPL_H


#include "IIntersection.h"
#include "iostream"
#include <vector>
#include <cmath>
#include "Eigen"

using namespace std;
using namespace Eigen;

class Intersection;

class Intersection : public IIntersection {
    //compute the intersection
public:
    enum Type
    {
        NoIntersection = 0,
        IntersectionOnLine = 1,
        IntersectionOnSegment = 2,
        IntersectionParallelOnLine = 3,
        IntersectionParallelOnSegment = 4,
    };
    enum Position
    {
        Begin = 0,
        Inner = 1,
        End = 2,
        Outer = 3
    };
private:
    double toleranceIntersection;

    Type type;
    Position positionIntersectionFirstEdge;
    Position positionIntersectionSecondEdge;


public:

    Intersection();
    ~Intersection();

    Vector2d resultParametricCoordinates;
    Vector2d lineOrigin;
    Vector2d rightHandSide;
    Matrix2d lineTangent;
    Vector2d originSeg;
    Vector2d endSeg;
    Vector2d pointInt;
    vector<Vector2d> intersectionPoints;
    Vector2d points;
    Vector2d vertices;


    void SetToleranceIntersection(const double& _tolerance) { toleranceIntersection = _tolerance; }
    void SetFirstSegment(const Vector2d& origin, const Vector2d& end) {lineTangent.col(0) = end - origin; lineOrigin = origin; }
    void SetSecondSegment(const Vector2d& origin, const Vector2d& end) {lineTangent.col(1) = origin - end; rightHandSide = origin - lineOrigin; }

    bool ComputeIntersection();

    Vector2d IntersectionFirstParametricCoordinate(const Vector2d& origin,const Vector2d& end) const {return (1 - resultParametricCoordinates(0)) * origin + resultParametricCoordinates(0) * end; }
    Vector2d IntersectionSecondParametricCoordinate(const Vector2d& origin,const Vector2d& end) const {return (1 - resultParametricCoordinates(1)) * origin + resultParametricCoordinates(0) * end; }
    const double& getToleranceIntersection() const {return toleranceIntersection; }

    void IntersectionPoint(const Vector2d& _originSeg, const Vector2d& _endSeg);


    const double& FirstParametricCoordinate() {return resultParametricCoordinates(0);}
    const double& SecondParametricCoordinate() {return resultParametricCoordinates(1);}

    const Position& PositionIntersectionInFirstEdge(){return positionIntersectionFirstEdge;}
    const Position& PositionIntersectionInSecondEdge(){return positionIntersectionSecondEdge;}
    const Type& TypeIntersection() {return type;}



};

#endif //POLYGONCUT_IINTERSECTIONIMPL_H