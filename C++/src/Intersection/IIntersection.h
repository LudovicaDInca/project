#ifndef POLYGONCUT_IINTERSECTION_H
#define POLYGONCUT_IINTERSECTION_H

#include "vector"
#include <iostream>
#include "Eigen"

using namespace std;
using namespace Eigen;

class IIntersection{
public:
    Vector2d pointInt;
    vector<Vector2d> intersectionPoints;

   virtual void IntersectionPoint(const Vector2d& _originSeg, const Vector2d& _endSeg) = 0;

};



#endif //POLYGONCUT_IINTERSECTION_H
//