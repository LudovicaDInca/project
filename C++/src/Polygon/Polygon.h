#ifndef POLYGONCUT_POLYGON_H
#define POLYGONCUT_POLYGON_H

#include "Intersection/IIntersection.h"
#include <iostream>
#include "Eigen"

using namespace std;

class Polygon {
private:
    IIntersection& _intersection;

    vector<Eigen::Vector2d> points;
    Vector2d vertices;

public:
    Polygon(IIntersection &intersection) : _intersection(intersection) {}
    pair<vector<Vector2d>,Vector2d> AddPoint(const std::vector<Eigen::Vector2d>& _points,const Vector2d& _vertices);
};

#endif //POLYGONCUT_POLYGON_H
