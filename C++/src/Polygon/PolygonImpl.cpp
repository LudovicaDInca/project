#include "Polygon.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

pair<vector<Vector2d>,Vector2d> Polygon::AddPoint(const std::vector<Eigen::Vector2d>& _points, const Vector2d& _vertices){
    points = _points;
    vertices = _vertices;
    unsigned int s = points.size() + 1;
    int v = vertices.size() + 1;
    for(int i = 0; i <= _intersection.intersectionPoints.size(); i++){
        points.push_back(_intersection.pointInt);
        vertices(v) = v;
        s++;
        v++;
    }
    return pair<vector<Vector2d>,Vector2d>(points, vertices);
}