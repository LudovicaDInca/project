//#ifndef __TEST_IINTERSECTIONIMPL_H
//#define __TEST_IINTERSECTIONIMPL_H

//#ifndef __TEST_POLYGONCUT_IINTERSECTIONIMPL_H
//#define __TEST_POLYGONCUT_IINTERSECTIONIMPL_H

#ifndef __TEST_POLYGONCUT_H
#define __TEST_POLYGONCUT_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include <Intersection/IIntersectionImpl.h>

#include "Eigen"
#include <Intersection/IIntersectionImpl.h>

using namespace testing;
using namespace std;

namespace IntersectionTesting {

    TEST(TestIntersection, TestComputeIntersectionFirst) {
        //FIRST TEST
        try {
            vector<Eigen::Vector2d> points{{1, 1},
                                           {5, 1},
                                           {5, 3.1},
                                           {1, 3.1}};
            vector<int> polygonVertices{0, 1, 2, 3};
            vector<Eigen::Vector2d> segment{{2, 1.2},
                                            {4, 3}};
            Eigen::Vector2d origin(2, 1.2);
            Eigen::Vector2d end(4, 3);


            Intersection intersectionNew;


            intersectionNew.SetFirstSegment(segment[0], segment[1]);
            intersectionNew.SetSecondSegment(points[0], points[1]);
            EXPECT_TRUE(intersectionNew.ComputeIntersection());
            EXPECT_EQ(intersectionNew.TypeIntersection(), Intersection::IntersectionOnLine);
            EXPECT_FLOAT_EQ(0.19444445, intersectionNew.SecondParametricCoordinate());//0.080600001
            EXPECT_FLOAT_EQ(-0.11111111, intersectionNew.FirstParametricCoordinate());
            intersectionNew.IntersectionPoint(origin, end);
            EXPECT_FLOAT_EQ(1.7777778, intersectionNew.intersectionPoints[0](0));//2.3888888
            EXPECT_FLOAT_EQ(1, intersectionNew.intersectionPoints[0](1));//1.55

            intersectionNew.SetFirstSegment(segment[0], segment[1]);
            intersectionNew.SetSecondSegment(points[2], points[3]);
            EXPECT_TRUE(intersectionNew.ComputeIntersection());
            EXPECT_EQ(intersectionNew.TypeIntersection(), Intersection::IntersectionOnLine);
            EXPECT_FLOAT_EQ(0.22222222, intersectionNew.SecondParametricCoordinate());
            EXPECT_FLOAT_EQ(1.0555556, intersectionNew.FirstParametricCoordinate());
            intersectionNew.IntersectionPoint(origin, end);
            EXPECT_FLOAT_EQ(4.1111112, intersectionNew.intersectionPoints[1](0));//4.11
            EXPECT_FLOAT_EQ(3.0999999, intersectionNew.intersectionPoints[1](1));//3.1

        }

        catch (const exception &exception) {
            EXPECT_THAT(std::string(exception.what()), Eq("Something goes wrong"));
        }
    }

    TEST(TestIntersection, TestComputeIntersectionSecond) {
        //SECOND TEST

        try {
            vector<Eigen::Vector2d> points{{2.5, 1},
                                           {4,   2.1},
                                           {3.4, 4.2},
                                           {1.6, 4.2},
                                           {1,   2.1}};
            vector<int> polygonVertices{0, 1, 2, 3, 4};
            vector<Eigen::Vector2d> segment{{1.4, 2.75},
                                            {3.6, 2.2}};
            Vector2d origin(1.4, 2.75);
            Vector2d end(3.6, 2.2);

            Intersection intersectionNew;

            intersectionNew.SetFirstSegment(segment[0], segment[1]);
            intersectionNew.SetSecondSegment(points[0], points[1]);
            EXPECT_TRUE(intersectionNew.ComputeIntersection());
            EXPECT_EQ(intersectionNew.TypeIntersection(), Intersection::IntersectionOnLine);
            EXPECT_FLOAT_EQ(1, intersectionNew.SecondParametricCoordinate());//-1.09
            EXPECT_FLOAT_EQ(1.1818181, intersectionNew.FirstParametricCoordinate());//1.1818181
            intersectionNew.IntersectionPoint(origin, end);
            EXPECT_FLOAT_EQ(4, intersectionNew.intersectionPoints[0](0));//3.5999999
            EXPECT_FLOAT_EQ(2.1, intersectionNew.intersectionPoints[0](1));//2.2

            intersectionNew.SetFirstSegment(segment[0], segment[1]);
            intersectionNew.SetSecondSegment(points[1], points[2]);
            EXPECT_TRUE(intersectionNew.ComputeIntersection());
            EXPECT_EQ(intersectionNew.TypeIntersection(), Intersection::IntersectionOnLine);
            EXPECT_FLOAT_EQ(1.035173e-16, intersectionNew.SecondParametricCoordinate());//0.35
            EXPECT_FLOAT_EQ(1.1818181, intersectionNew.FirstParametricCoordinate());
            intersectionNew.IntersectionPoint(origin, end);
            EXPECT_FLOAT_EQ(4, intersectionNew.intersectionPoints[1](0));
            EXPECT_FLOAT_EQ(2.0999999, intersectionNew.intersectionPoints[1](1));

            intersectionNew.SetFirstSegment(segment[0], segment[1]);
            intersectionNew.SetSecondSegment(points[3], points[4]);
            EXPECT_TRUE(intersectionNew.ComputeIntersection());
            EXPECT_EQ(intersectionNew.TypeIntersection(), Intersection::IntersectionOnLine);
            EXPECT_FLOAT_EQ(0.66666669, intersectionNew.SecondParametricCoordinate());
            EXPECT_FLOAT_EQ(-0.090909094, intersectionNew.FirstParametricCoordinate());
            intersectionNew.IntersectionPoint(origin, end);
            EXPECT_FLOAT_EQ(1.2, intersectionNew.intersectionPoints[2](0));
            EXPECT_FLOAT_EQ(2.8, intersectionNew.intersectionPoints[2](1));
        }
        catch (const exception &exception) {
            EXPECT_THAT(std::string(exception.what()), Eq("Something goes wrong"));
        }
    }

    TEST(TestIntersection, TestComputeIntersectionThird) {
        try {
            vector<Eigen::Vector2d> points{{1.5, 1.0},
                                           {5.6, 1.5},
                                           {5.5, 4.8},
                                           {4.0, 6.2},
                                           {3.2, 4.2},
                                           {1.0, 4.0}};
            vector<int> polygonVertices{0, 1, 2, 3, 4, 5};
            vector<Eigen::Vector2d> segment{{2,   3.7},
                                            {4.1, 5.9}};
            Eigen::Vector2d origin(2, 3.7);
            Eigen::Vector2d end(4.1, 5.9);


            Intersection intersectionNew;

            intersectionNew.SetFirstSegment(segment[0], segment[1]);
            intersectionNew.SetSecondSegment(points[2], points[3]);
            EXPECT_TRUE(intersectionNew.ComputeIntersection());
            EXPECT_EQ(intersectionNew.TypeIntersection(), Intersection::IntersectionOnLine);
            EXPECT_FLOAT_EQ(0.86378205, intersectionNew.SecondParametricCoordinate());
            EXPECT_FLOAT_EQ(1.0496795, intersectionNew.FirstParametricCoordinate());
            intersectionNew.IntersectionPoint(origin, end);
            EXPECT_FLOAT_EQ(4.2043271, intersectionNew.intersectionPoints[0](0));
            EXPECT_FLOAT_EQ(6.009295, intersectionNew.intersectionPoints[0](1));

            intersectionNew.SetFirstSegment(segment[0], segment[1]);
            intersectionNew.SetSecondSegment(points[3], points[4]);
            EXPECT_TRUE(intersectionNew.ComputeIntersection());
            EXPECT_EQ(intersectionNew.TypeIntersection(), Intersection::IntersectionOnSegment);
            EXPECT_FLOAT_EQ(0.34836066, intersectionNew.SecondParametricCoordinate());//0.65
            EXPECT_FLOAT_EQ(0.81967211, intersectionNew.FirstParametricCoordinate());
            EXPECT_LE(intersectionNew.FirstParametricCoordinate(), 1.0);
            EXPECT_GE(intersectionNew.FirstParametricCoordinate(), 0.0);
            intersectionNew.IntersectionPoint(origin, end);
            EXPECT_FLOAT_EQ(3.7213116, intersectionNew.intersectionPoints[1](0));
            EXPECT_FLOAT_EQ(5.5032787, intersectionNew.intersectionPoints[1](1));

            intersectionNew.SetFirstSegment(segment[0], segment[1]);
            intersectionNew.SetSecondSegment(points[4], points[5]);
            EXPECT_TRUE(intersectionNew.ComputeIntersection());
            EXPECT_EQ(intersectionNew.TypeIntersection(), Intersection::IntersectionOnSegment);
            EXPECT_FLOAT_EQ(0.35972852, intersectionNew.SecondParametricCoordinate());
            EXPECT_FLOAT_EQ(0.19457014, intersectionNew.FirstParametricCoordinate());
            EXPECT_LE(intersectionNew.FirstParametricCoordinate(), 1.0);
            EXPECT_GE(intersectionNew.FirstParametricCoordinate(), 0.0);
            intersectionNew.IntersectionPoint(origin, end);
            EXPECT_FLOAT_EQ(2.4085972, intersectionNew.intersectionPoints[2](0));
            EXPECT_FLOAT_EQ(4.1280541, intersectionNew.intersectionPoints[2](1));

            intersectionNew.SetFirstSegment(segment[0], segment[1]);
            intersectionNew.SetSecondSegment(points[5], points[0]);
            EXPECT_TRUE(intersectionNew.ComputeIntersection());
            EXPECT_EQ(intersectionNew.TypeIntersection(), Intersection::IntersectionOnLine);
            EXPECT_FLOAT_EQ(0.38243243, intersectionNew.SecondParametricCoordinate());//0.62
            EXPECT_FLOAT_EQ(-0.38513514, intersectionNew.FirstParametricCoordinate());
            intersectionNew.IntersectionPoint(origin, end);
            EXPECT_FLOAT_EQ(1.1912162, intersectionNew.intersectionPoints[3](0));
            EXPECT_FLOAT_EQ(2.8527026, intersectionNew.intersectionPoints[3](1));


        }
        catch (const exception &exception) {
            EXPECT_THAT(std::string(exception.what()), Eq("Something goes wrong"));
        }

    }

}


#endif // __TEST_POLYGONCUT_H